# -*- coding: utf-8 -*-
##########################################################################
# Author      : Knacktechs Solutions (<https://knacktechs.com/>)
# Copyright(c): 2017-Present Knacktechs Solutions 
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.knacktechs.com/license.html/>
##########################################################################
{
    "name":  "Employee Appraisal",
    "summary":  "",
    "category":  "Employee",
    "version":  "1.2.0",
    "sequence":  1,
    "author":  "",
    "license":  "Other proprietary",
    "website":  "",
    "description":  """Employee Appraisal""",
    "depends":  ['base','hr','oh_appraisal'],
    'data': [
        'security/access_rights_group.xml',
        'security/ir.model.access.csv',
        'views/hr_appraisal_view.xml',
        'views/employee_view.xml',
        'views/employee_appraisal_view.xml',
        'views/employee_objective_view.xml',
        'views/res_partner_view.xml',
#        'views/mail_view.xml',
#        'views/survey_template_view.xml',
        'data/cron.xml',
    ],
    "application":  True,
    "installable":  True,
    "auto_install":  False,
}
