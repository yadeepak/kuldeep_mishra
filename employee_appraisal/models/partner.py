    
from odoo import fields, models

class Partner(models.Model):
    _inherit='res.partner'
    
    designation=fields.Selection([('manager','Manager'),('employee','Employee'),('direct_manager','Direct Manager'),('ceo','CEO')],'Designation',default='employee')
    
    
