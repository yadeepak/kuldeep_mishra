from odoo import fields, models,api

class HRAppraisal(models.Model):
    _inherit = 'hr.appraisal' 
    
    @api.depends('hr_criteria_ids','hr_criteria_ids.appraisal_value','annual_score','behaviour_score')
    def _final_result(self):
        rating=0.0
        final=0.0
        for appraisal in self:
            if appraisal.hr_criteria_ids:
                rating=sum(value.appraisal_value for value in appraisal.hr_criteria_ids)/len(appraisal.hr_criteria_ids)
                final=appraisal.annual_score*0.5+appraisal.behaviour_score*0.3+rating*0.2
#                print('0000000appraisal.hr_rating',rating,appraisal.annual_score,appraisal.behaviour_score)
            appraisal.hr_rating=rating
            appraisal.final_result=final
            
    question_type=fields.Selection([('numerical_box','Numerical Value'),('text_value','Text Value')],'Type',default='numerical_box')
    annual_score=fields.Float('Annual Plan Score(50%)')
    behaviour_score=fields.Float('Behavioral Competences Score(30%)')
    hr_criteria_ids = fields.One2many('hr.criteria', 'appraisal_id', 'Appraisal Id')
    final_result=fields.Float('Final Result',compute='_final_result')
    hr_rating=fields.Float('Hr Score(20%)',compute='_final_result')
    
    @api.onchange('emp_id')
    def onchange_employee_id(self):
        print('----------')
        if self.emp_id:
            question_line=[]
            common_question_line=[]
            survey_page=[]
            survey_page_obj=self.env['survey.page']
            survey_survey_obj=self.env['survey.survey']
            appraisal_id=self.env['employee.appraisal'].search([('employee_id','=',self.emp_id.id)])#,('state','in','done')
#            print('---appraisal_id',appraisal_id)
            if appraisal_id:
#           #Set the value based on on employee appraisal form
                self.appraisal_deadline=appraisal_id.due_date
                self.hr_manager=True
                self.hr_emp=True
                self.hr_colloborator=True
                self.hr_colleague=True
    #           #set the objectve as a question in supplementary question form
                for question in appraisal_id.objective_ids:
                    survey_question_val={
                    'question':question.objective_id.name,
                    'type':self.question_type,
                    }
                    question_line.append((0,0,survey_question_val))
                survey_page_val={
                    'title':'Annual Plan',
                    'question_ids':question_line,
                }
                survey_page.append((0,0,survey_page_val))
                #Serach for the common questions
                survey_page_id=survey_page_obj.search([('title','=','Behavioral Competencies')])
                if survey_page_id:
                    for question in survey_page_id[0].question_ids:
                        survey_question_common_val={
                        'question':question.question,
                        'type':question.type,
                        }
                        common_question_line.append((0,0,survey_question_common_val))
                survey_page_common_val={
                    'title':'Behavioral Competencies',
                    'question_ids':common_question_line,
                }
                survey_page.append((0,0,survey_page_common_val))
                survey_val={
                 'title':self.emp_id.name+ ' '+'Appraisal',
                 'page_ids':survey_page,
                }
    #            Create the survey object with commom and supplementary questions
                survey_id=survey_survey_obj.create(survey_val)
                self.manager_survey_id=survey_id
                self.emp_survey_id=survey_id
                self.colloborator_survey_id=survey_id
                self.colleague_survey_id=survey_id
            
#    @api.multi
#    def action_start_appraisal(self):
#        """ This function will start the appraisal by sending emails to the corresponding employees
#            specified in the appraisal#"""
#        send_count = 0
#        appraisal_reviewers_list = self.fetch_appraisal_reviewer()
#        for appraisal_reviewers, survey_id in appraisal_reviewers_list:
#            for reviewers in appraisal_reviewers:
#                url = survey_id.public_url
#                response = self.env['survey.user_input'].create(
#                    {'survey_id': survey_id.id, 'partner_id': reviewers.user_id.partner_id.id,
#                     'appraisal_id': self.ids[0],  'deadline': self.appraisal_deadline, 'email': reviewers.user_id.email})
#                token = response.token
#                if token:
#                    url = url + '/' + token
#                    mail_content = "Dear " + reviewers.name + "," + "<br>Please fill out the following survey " \
#                                   "related to " + self.emp_id.name + "<br>Click here to access the survey.<br>" + \
#                                   str(url) + "<br>Post your response for the appraisal till : " + str(self.appraisal_deadline)
#                    values = {'model': 'hr.appraisal',
#                              'res_id': self.ids[0],
#                              'subject': survey_id.title,
#                              'body_html': mail_content,
#                              'parent_id': None,
#                              'email_from': self.env.user.email or None,
#                              'auto_delete': True,
#                             }
#                    values['email_to'] = reviewers.work_email
#                    values['token'] = token
#                    result = self.env['mail.mail'].create(values)._send()
#                    if result is True:
#                        send_count += 1
#                        self.write({'tot_sent_survey': send_count})
#                        rec = self.env['hr.appraisal.stages'].search([('sequence', '=', 2)])
#                        self.state = rec.id
#                        self.check_sent = True
#                        self.check_draft = False
#
#        if self.hr_emp and self.emp_survey_id:
#            self.ensure_one()
#            if not self.response_id:
#                response = self.env['survey.user_input'].create(
#                    {'survey_id': self.emp_survey_id.id, 'partner_id': self.emp_id.user_id.partner_id.id,
#                     'appraisal_id': self.ids[0], 'deadline': self.appraisal_deadline, 'email': reviewers.user_id.email})
#                self.response_id = response.id
#            else:
#                response = self.response_id
#            return self.emp_survey_id.with_context(survey_token=response.token).action_start_survey()
    
#    It will do the calculation part
    @api.multi
    def action_show_marks(self):
        ratings=[]
        final_annual_score=0.0
        behaviour_score=0.0
       
        survey_obj=self.env['survey.user_input']
#        emo_objective_obj=self.env['employee.objective']
#        mail_id=self.env['mail.mail'].search([('res_id','=',self.id)])
#        print('--------mail_object',mail_id)
#        for each_mail in mail_id:
#            search the servey id based on token used in mail
        survey_id=survey_obj.search([('appraisal_id','=',self.id)])
        
#        print('survey_idoooooooooo',survey_id)
#            annual_plan and behaviour calculation is here based on ratings
#        fiwye
        for survey in survey_id:
#            print('surveysurveysurvey-',survey)
            counter=1
            plan_count=0
            behaviour_count=0
            if survey.user_input_line_ids:
                annual_plan=0.0
                behaviour_com=0.0
                for page in survey.user_input_line_ids:
        #                objective_id=emo_objective_obj.search([('name','=',page.question_id.question)])
        #                print('objective_idobjective_id',objective_id)
                    if counter==1:
                        name=page.page_id.title
                    if name==page.page_id.title:
                        plan_count+=1
#                        print('plan_count',plan_count)
                        annual_plan+=page.value_number
#                        print('page.value_number--', page.value_number)
#                        print('annual_planan--',annual_plan)
                    else:
                        behaviour_count+=1
#                        print('behaviour_count',plan_count)
                        behaviour_com+=page.value_number
#                        print('value_number--',page.value_number)
                    counter+=1
                if survey.partner_id.designation=='manager':
#                    print('manager')
#                   print('--------plan_count',plan_count)
#                    print('annual_pan[[[[[[[',annual_plan)
#                    print('--------annual_plan',annual_plan)
                    annual_plan=annual_plan/plan_count*0.4
#                    print('annual_planssss-----',annual_plan)
                    behaviour_com=behaviour_com/behaviour_count*0.4
#                    print('behaviour_countbehaviour_count',behaviour_count)
#                    print('behaviour_combehaviour_com',behaviour_com)
                elif survey.partner_id.designation=='direct_manager':
#                    print('direct manager')
#                    print('--------plan_count',plan_count)
                    annual_plan=annual_plan/plan_count*0.3
#                    print('annual_plan-----',annual_plan)
                    behaviour_com=behaviour_com/behaviour_count*0.3
                elif survey.partner_id.designation=='ceo':
#                    print('ceo')
#                    print('--------annual_plan',annual_plan)
                    annual_plan=annual_plan/plan_count*0.2
#                    print('annual_plan-----',annual_plan)
                    behaviour_com=behaviour_com/behaviour_count*0.2
                elif survey.partner_id.designation=='employee':
#                    print('employee')
#                    print('--------annual_plan',annual_plan)
                    annual_plan=annual_plan/plan_count*0.1
#                    print('annual_plan-----',annual_plan)
                    behaviour_com=behaviour_com/behaviour_count*0.1
                final_value={'annual_plan':annual_plan,'behaviour_com':behaviour_com}
                ratings.append((final_value))
#                print('000000000000000ratings',ratings)
#                wudgqd
#        print('000000000000000ratings',ratings)
        for value in ratings:
#            print('valuevaluevalue---',value['annual_plan'])
#            print('valuevaluevalue---',value['behaviour_com'])
            final_annual_score+=value['annual_plan']
            behaviour_score+=value['behaviour_com']
#            print('final_annual_score',final_annual_score)
#            print('behaviour_score',behaviour_score)
        self.annual_score=final_annual_score
        self.behaviour_score=behaviour_score
        
        
        
class HrCriteria(models.Model):
    _name='hr.criteria'
    
    appraisal_id = fields.Many2one('hr.appraisal','Appraisal Id')
#    name=fields.Char('Name')
    title= fields.Text(string='Criteria')
    desc= fields.Text(string='Description')
    weight= fields.Float(string='Weight')
    appraisal_value= fields.Float(string='Appraisal')