from odoo import fields, models,api
#from odoo.exceptions import ValidationError

class Employee(models.Model):
    _inherit = 'hr.employee' 
    
#    last_name=fields.Char('Last Name')
    due_date=fields.Date('Due Date')
    plan_count = fields.Integer(compute='_compute_plan_count', string='Employee Plans')
    
    def _compute_plan_count(self):
        # read_group as sudo, since employee plan count is displayed on form view
        employee_plan = self.env['employee.appraisal'].sudo().read_group([('employee_id', 'in', self.ids)], ['employee_id'], ['employee_id'])
        result = dict((data['employee_id'][0], data['employee_id_count']) for data in employee_plan)
        for employee in self:
            employee.plan_count = result.get(employee.id, 0)


 
        