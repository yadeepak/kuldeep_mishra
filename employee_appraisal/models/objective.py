from odoo import fields, models

class EmployeeObjectives(models.Model):
    _name='employee.objective'
    
    name=fields.Char('Name')
    title= fields.Char(string='Objective Title')
    desc= fields.Text(string='Objective Description')
    weight= fields.Float(string='Objective Weight')
    kpis= fields.Char(string='KPIs')
    kpis_id=fields.Many2many('objective.kpis','employee_bjective_kpis','kips_id','objective_id',string="KPIs")
    

class ObjectiveKpis(models.Model):
    _name='objective.kpis'
    
    name=fields.Char('Name')
    desc= fields.Char(string='Description')

    