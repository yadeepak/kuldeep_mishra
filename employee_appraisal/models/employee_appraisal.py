from odoo import _
from odoo import api
from odoo import fields
from odoo import models
from odoo.exceptions import UserError
from openerp import models, api,_
from datetime import date
from dateutil.relativedelta import relativedelta

class EmployeeAppraisal(models.Model):
    _name = 'employee.appraisal'
    _rec_name = 'employee_id'
    
    employee_id = fields.Many2one('hr.employee', string='First Name')
#    last_name = fields.Char(string='Last Name')
    team_id = fields.Many2one('hr.department', string='Team')
    due_date = fields.Date(string='Due Date')
    appraisal_year = fields.Selection([('half-yearly', 'Half Yearly'), ('yearly', 'Yearly')])
    objective_ids = fields.One2many('goals.objectives', 'appraisal_id', 'Objective Id')
    state = fields.Selection([
                             ('employee', 'Created'),
                             ('manager', 'Requested to Manager'),
                             ('confirm', 'Confirmed'),
                             ('done', 'Done'),
                             ('decline', 'Declined'),
                             ], string='Status', index=True, readonly=True, default='employee')
        
        
    @api.multi
    def action_send_to_manager(self):
        self.write({'state': 'manager'})
    
    @api.multi
    def action_confirm(self):
        self.write({'state': 'confirm'})
    
    @api.multi
    def action_decline(self):
        self.write({'state': 'decline'})
        
    @api.multi
    def set_to_employee(self):
        self.write({'state': 'employee'})
        
    @api.model
    def create(self, vals):
        weight = 0.0
        if vals.get('objective_ids'):
            for objective in vals.get('objective_ids'):
                if objective[2] and objective[2].get('weight', False):
                    weight += objective[2].get('weight')
            if weight != 100:
                raise UserError(_('Weight value should be equal to 100.'))
        return super(EmployeeAppraisal, self).create(vals)
    
    
    @api.model
    def cron_employee_plan_to_manager(self):
        current_date = fields.Date.context_today(self)
        appraisal_id=self.env['employee.appraisal'].search([('state','=','confirm'),('due_date','=',current_date)])
        for appraisal in appraisal_id:
            appraisal.write({'state':'done'})
            
    @api.onchange('appraisal_year')
    def on_change_year(self):
        if self.appraisal_year=='half-yearly':
            six_months = date.today() + relativedelta(months=+6)
            self.due_date=six_months
        if self.appraisal_year=='yearly':
            one_year = date.today() + relativedelta(months=+12)
            self.due_date=one_year
    
class GoalsObjectives(models.Model):
    _name = 'goals.objectives'
    
    appraisal_id = fields.Many2one('employee.appraisal', 'Appraisal Id')
    objective_id = fields.Many2one('employee.objective')
    title = fields.Char(string='Title')
    desc = fields.Text(string='Description')
    weight = fields.Float(string='Weight')
    kpis_ids = fields.Many2many('objective.kpis', 'appraisal_objective_kpis_id', 'kip_id', 'appraisal_ids')
    
    @api.onchange('objective_id')
    def onchnage_objective_id(self):
        if self.objective_id:
            self.title = self.objective_id.title
            self.desc = self.objective_id.desc
            self.weight = self.objective_id.weight
            self.kpis_ids = [(6, 0, [kpis.id for kpis in self.objective_id.kpis_id])]
            
            
        