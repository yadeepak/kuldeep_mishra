# -*- coding: utf-8 -*-
##############################################################################
#
# Odoo, Open Source Management Solution
# Copyright (C) 2016 KnackTechs
# Author : www.knacktechs.com
#
#############################################################################

from . import employee
from . import appraisal
from . import objective    
from . import employee_appraisal    
from . import partner    
#from . import mail    

