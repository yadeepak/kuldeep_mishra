# -*- coding: utf-8 -*-
##########################################################################
# Author      : 
# Copyright(c): 2017-Present Knacktechs Solutions 
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.knacktechs.com/license.html/>
##########################################################################
{
    "name":  "HRMS Reports",
    "summary":  "Knacktechs Solutions",
    "category":  "HRMS",
    "version":  "1.2.0",
    "sequence":  1,
    "author":  "",
    "website":  "",
    "description":  """It will generate the reports related to HRMS""",
    "depends":  ['base','hr','hr_payroll'],
    'data': [
            'reports/new_hire_employee_template.xml',
            'reports/employee_termination_template.xml',
            'reports/leave_administration.xml',
            'reports/employee_information.xml',
            'reports/employee_number_template.xml',
            'reports/leave_type.xml',
            'reports/employee_change_history.xml',
            'reports/report.xml',
            'wizards/new_hire_employee_view.xml',
            'wizards/termination_view.xml',
            'wizards/employee_number_view.xml',
            'wizards/leave_administration_view.xml',
            'wizards/employee_info_view.xml',
            'wizards/leave_type_view.xml',
            'wizards/employee_change_view.xml',
    ],
    "application":  True,
    "installable":  True,
    "auto_install":  False,
}
