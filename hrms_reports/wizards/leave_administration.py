from odoo import api,fields,models,_

class LeaveType(models.TransientModel):
    _name = 'leave.type'

    date_from=fields.Date('Date From',default=fields.Date.today())    
    date_to=fields.Date('Date To')    
    company_id = fields.Many2one('res.company', string='Company', change_default=True,
    default=lambda self: self.env['res.company']._company_default_get('leave.type'))

    
    @api.multi
    def print_employee_type_leave_report(self):
        return self.env.ref('hrms_reports.employee_leave_type_report').report_action(self)

    @api.multi
    def employee_leave_type(self):
        holiday_obj=self.env['hr.holidays']
        vals=[]
        holiday_id=holiday_obj.search([('create_date','>=',self.date_from),('create_date','<=',self.date_to)])
        for holiday in holiday_id:
            vals.append(holiday)
        return vals
    
    


        
        