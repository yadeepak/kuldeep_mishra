# -*- coding: utf-8 -*-
##############################################################################
#
# Odoo, Open Source Management Solution
# Copyright (C) 2016 KnackTechs
# Author : www.knacktechs.com
#
#############################################################################

from . import new_hire_employee
from . import termination
from . import leave_administration
from . import employee_info
from . import employee_number
from . import leave_type
from . import employee_change

