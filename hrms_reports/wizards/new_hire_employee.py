from odoo import api,fields,models,_

class NewHire(models.TransientModel):
    _name = 'new.hire.employee'

    date_from=fields.Date('Date From',default=fields.Date.today())    
    date_to=fields.Date('Date To')    
    company_id = fields.Many2one('res.company', string='Company', change_default=True,
    default=lambda self: self.env['res.company']._company_default_get('new.hire.employee'))

    
    @api.multi
    def print_new_hire_report(self):
        return self.env.ref('hrms_reports.new_hire_employee_report').report_action(self)

    @api.multi
    def new_hire_contracts(self):
        cont_obj=self.env['hr.contract']
        vals=[]
        contracts=cont_obj.search([('date_start','>=',self.date_from),('date_start','<=',self.date_to),('state','=','draft')])
        for contract in contracts:
            vals.append(contract)
        return vals
    
    


        
        