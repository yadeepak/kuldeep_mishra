from odoo import api,fields,models,_

class LeaveTypeType(models.TransientModel):
    _name = 'leave.type.types'

    company_id = fields.Many2one('res.company', string='Company', change_default=True,
    default=lambda self: self.env['res.company']._company_default_get('leave.type'))

    
    @api.multi
    def print_employee_leave_type_report(self):
        return self.env.ref('hrms_reports.employee_type_of_leave_report').report_action(self)

    @api.multi
    def employee_leave_type_details(self):
        holiday_obj=self.env['hr.holidays']
        vals=[]
        holiday_id=holiday_obj.search([('state','in',('draft','confirm','validate1','validate'))])
        for holiday in holiday_id:
            vals.append(holiday)
        return vals
    
    


        
        