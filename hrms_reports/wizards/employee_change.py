from odoo import api,fields,models,_

class EmployeeChange(models.TransientModel):
    _name = 'employee.change'

    date_from=fields.Date('Date From',default=fields.Date.today())    
    date_to=fields.Date('Date To')    
    company_id = fields.Many2one('res.company', string='Company', change_default=True,
    default=lambda self: self.env['res.company']._company_default_get('employee.change'))

    
    @api.multi
    def print_employee_change_report(self):
        return self.env.ref('hrms_reports.employee_change_report').report_action(self)

    @api.multi
    def employee_changed(self):
        cont_obj=self.env['hr.employee']
        vals=[]
        contracts=cont_obj.search([('write_date','>=',self.date_from),('write_date','<=',self.date_to)])
        for contract in contracts:
            vals.append(contract)
        return vals
    
    


        
        