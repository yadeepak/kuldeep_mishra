from odoo import api,fields,models,_

class Termination(models.TransientModel):
    _name = 'employee.termination'

    date_from=fields.Date('Date From',default=fields.Date.today())    
    date_to=fields.Date('Date To')    
    company_id = fields.Many2one('res.company', string='Company', change_default=True,
    default=lambda self: self.env['res.company']._company_default_get('employee.termination'))

    
    @api.multi
    def print_employee_terminated_report(self):
        return self.env.ref('hrms_reports.employee_termination_report').report_action(self)

    @api.multi
    def employee_terminated(self):
        cont_obj=self.env['hr.contract']
        vals=[]
        contracts=cont_obj.search([('date_end','>=',self.date_from),('date_end','<=',self.date_to),('state','in',('cancel','close'))])
        for contract in contracts:
            vals.append(contract)
        return vals
    
    


        
        