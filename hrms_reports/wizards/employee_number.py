from odoo import api,fields,models,_

class EmployeeNumber(models.TransientModel):
    _name = 'employee.number'

    company_id = fields.Many2one('res.company', string='Company', change_default=True,
    default=lambda self: self.env['res.company']._company_default_get('employee.number'))

    
    @api.multi
    def print_employee_number_report(self):
        return self.env.ref('hrms_reports.employee_numbers_report').report_action(self)

    @api.multi
    def employee_number_detials(self):
        cont_obj=self.env['hr.employee']
        vals=[]
        employees=cont_obj.search([('active','=',True)])
        for emp in employees.sorted(key=lambda r: r.department_id):
            vals.append(emp)
        return vals
    
    


        
        