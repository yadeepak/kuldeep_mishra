from odoo import api,fields,models,_

class EmployeeInfo(models.TransientModel):
    _name = 'employee.info'

    company_id = fields.Many2one('res.company', string='Company', change_default=True,
    default=lambda self: self.env['res.company']._company_default_get('employee.info'))

    
    @api.multi
    def print_employee_info_report(self):
        return self.env.ref('hrms_reports.employee_information_report').report_action(self)

    @api.multi
    def employee_detials(self):
        cont_obj=self.env['hr.employee']
        vals=[]
        employees=cont_obj.search([('active','=',True)])
        for contract in employees:
            vals.append(contract)
        return vals
    
    


        
        