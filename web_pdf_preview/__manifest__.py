# -*- coding: utf-8 -*-
##########################################################################
# Author      : Knacktechs Solutions (<https://knacktechs.com/>)
# Copyright(c): 2017-Present Knacktechs Solutions 
# All Rights Reserved.
#
#
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#
# You should have received a copy of the License along with this program.
# If not, see <https://www.knacktechs.com/license.html/>
##########################################################################

{
    "name": "Web PDF Report Preview & Print",
    'version': '1.2.0',
    'category': 'Web',
    'description': """Web PDF Report Preview & Print

Preview & Print PDF report in your browser.

* For IE, Adobe Reader is required.
* For Chrome , nothing is requried.
* For Firefox, Adobe Reader is required.


If your brower prevented pop-up window, you should allow it.

    """,
    'author': 'Knacktechs Solutions',
    'website': 'www.knacktechs.com',
    'license': 'AGPL-3',
    'depends': ['web'],
    'data': [
        'views/web_pdf_preview.xml',
    ],

}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
