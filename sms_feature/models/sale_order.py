from odoo import models, fields, api,_
from odoo.exceptions import UserError

class SaleOrder(models.Model):
    _inherit='sale.order'
    
    @api.multi
    def action_confirm(self):
        res_config=self.env['res.config.settings']
        self.partner_id.mobile
        if self.partner_id.mobile:
            res_config.sms_api(self.partner_id.mobile,'Dear '+self.partner_id.name+',\nYour order of amount Rs.'+str(self.amount_total)+' has been confirmed.')
        else:
            raise UserError(_('Please set mobile number field for the customer.'))
        res=super(SaleOrder,self).action_confirm()
        return res
    
#    
class StockPicking(models.Model):
    _inherit='stock.picking'
    
    @api.multi
    def button_validate(self):
        res_config=self.env['res.config.settings']
        self.partner_id.mobile
        if self.partner_id.mobile:
            res_config.sms_api(self.partner_id.mobile,'Dear '+self.partner_id.name+' your delivery has been Done.')
        else:
            raise UserError(_('Please set mobile number field for the customer.'))
        res=super(StockPicking,self).button_validate()
        return res
#    
