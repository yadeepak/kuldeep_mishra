# -*- coding: utf-8 -*-
from odoo import models, fields, api
import requests

# http://smshorizon.co.in/api/sendsms.php?user=nbicorp&apikey=wuT7oUdntWSla8GqAKiT&mobile=+917000527278&message=Thanks&senderid=SMSTXT&type=txt



class MessageSend(models.TransientModel):
    _inherit = 'res.config.settings'
    
    url1 = fields.Char('SMS API Url',
        default='http://smshorizon.co.in/api/sendsms.php'
    )

    sms_key = fields.Char('Auth Key',
        default='wuT7oUdntWSla8GqAKiT'
    )
    sms_sender = fields.Char('Sender Id',
        default='SMSTXT'
    )

    def get_default_url1(self, fields):
        l1=self.search([])
        if not l1:
            return {'url1':'http://smshorizon.co.in/api/sendsms.php'}
        obj=self.browse(l1)
        return {'url1':obj[-1].url1}

    def get_default_sms_key(self,fields):
        l2=self.search([])
        if not l2:
            return{'sms_key':'wuT7oUdntWSla8GqAKiT'}
        obj2=self.browse(l2)
        return{'sms_key': obj2[-1].sms_key}

    def get_default_sms_sender(self, fields):
        l1 = self.search([])
        if not l1:
            return {'sms_sender': 'SMSTXT'}
        obj = self.browse(l1)
        return {'sms_sender': obj[-1].sms_sender}

    def sms_api(self, mobilenumber, msg):
        """This method take mobilenumber and 
        message and call the API to send message, 
        authkey and url should be configured"""
        obj = self.search([])
        if obj :
            sms_sender = obj[-1].sms_sender
            sms_key = obj[-1].sms_key
            url1 = obj[-1].url1
            sms = requests.get(
                url1+'?user=nbicorp&apikey='+sms_key+'&mobile='+mobilenumber+'&message='+msg+'&senderid='+sms_sender+'&type=txt')

